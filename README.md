# Simple todo-list demo

> *Written by: Joshua Fuentes <joshuarpf@gmail.com>*

This is a simple todo-list web application built for an exercise, demonstrating domain knowledge in scope. It was **never** intended for a production relese, hence the environments created are designed to be for development. 

Typscript was used for most of the coding, with a little javascript invovled. UI/UX is using the material-ui toolkit, and the database is using postgres. 

## Developer Notes
As much as I want to work on this, I did not have the luxury of time to focus this task alone. Hence, there are a few missing features that are in the original requirements. I will probably continue to play around this and enhance it, when time permits. 

Here are the points that I want to demonstrate in this exercise:
1. Domain knowledge in nodejs
2. Domain knowledge in reactjs 
3. Domain knowlege in containerization
4. Domain knowlege utilizing existing packages for rapid development
5. Use of redux ( state management )
6. Good coding techniques and design. 

There are a lot of improvements that I have in mind at this point, but unfortunately, I do not have the time to implement them. In a real-world scenario, these will be amongst the things that I will have impelmented. 

1. Proper object oriented approach - which also means that there will a biger structure of data to play with, potentially including relationships and heriarchy. 
2. Adherance to the SOLID principles
3. Independent modules will be created to allow things like support for multiple database engines. 

That will be all for now. Please feel free to comment. 

## Limitations
Here are the things that are lacking: 
1. Validation - I was going to use the **express-validator** for the back end, combined with HTML 5 validations in the front end. 
2. Additional data relationships - for assignee 
3. Pagination


## Running the application

*Requirements*

 1. Linux or Linux based OS
 2. docker 
 3. docker-compose

*Installation guide*
1. Clone  or open the repository bundle 
2. Ensure that you are not using ports 5432 ( postres ) , 3000 and 8080. If you do wish to change these, please feel free to look into the docker-compose.yml file, and the Dockerfiles to modify the configurations
3. Go to the main directory
4. Run "**make**" - It will build the environments and run the app for you. Once finished, you will notice that the logs are displayed, these are all intentional as we are in a development inironment. 

*How to stop*
Simply run **make clean**, and it will do its thing. It will remove containers, images, volumes and network used. 