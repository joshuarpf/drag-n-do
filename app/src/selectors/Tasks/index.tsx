import axios from 'axios'

/**
 * Class that contains the operations for "tasks"
 */
export default class TasksSelector {
	static delete = async(id: number): Promise<any> => {
		
		return await axios.post('http://10.6.0.7:8080/remove', { id })
			.then(result => {
				return result
			}).catch(error => {
				console.error(`AXIOS ERROR: ${error}`)
			})
	}

	/**
	 * Fetches all the records
	 * @param itemsPerPage 
	 */
	static fetch = async (itemsPerPage?: number): Promise<any> => {
		return await axios.get('http://10.6.0.7:8080')
		.then(results => {
			return results.data
		}).catch(error => {
			console.error(`AXIOS ERROR: ${error}`)
		})
	}

	/**
	 * Calls to update the sorting of the task list
	 * @param task 
	 */
	static sort = async(newOrder: {}): Promise<any> => {
		return await axios.post('http://10.6.0.7:8080/sort', newOrder)
		.then(results => {
			return results.data
		}).catch(error => {
			console.log(`AXIOS ERROR: ${error}`)
		})
	}


	/**
	 * Call to update a record
	 * @param task 
	 */
	static update = async(task: {}): Promise<any> => {
		return await axios.post('http://10.6.0.7:8080/save', task)
		.then(results => {
			return results.data
		}).catch(error => {
			console.log(`AXIOS ERROR: ${error}`)
		})
	}
}