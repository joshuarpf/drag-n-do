import React, { useState } from 'react'
import { Draggable } from 'react-beautiful-dnd'

import TasksAction from '../../stores/tasks/TasksAction'
import { useDispatch } from 'react-redux'
import EditField from '../Forms/EditField'
import TasksActions from '../../stores/tasks/TasksAction'

// Material UI
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box'
import Checkbox from '@material-ui/core/Checkbox';

const Item = (props: any) => {

	const [isComplete, toggleIsComplete] = useState((props.task.task_status > 0 )? true:false)

	const dispatch = useDispatch()

	const handleDelete = (id: number) => {
		dispatch(TasksAction.requestDelete(id))
	}

	const handleChange = () => {
		toggleIsComplete(!isComplete)
		dispatch(TasksActions.update({ task_status: (isComplete)? 0:1 , id: props.task.id, sort_order: props.task.sort_order }))
	}

	return(
		<Draggable draggableId={"#draggable-" + props.task.id} index={props.index}>
			{(provided: any) => 
				<Box  display="flex" flexDirection="row" p={1} m={1} bgcolor="#f2f5f3" ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
					<Box p={1} bgcolor="#f2f5f3">
						<Button size={"small"} onClick={()=> { handleDelete(props.task.id) }} color="secondary" variant="contained">Delete</Button>
					</Box>
					<Box p={1} bgcolor="#f2f5f3" >
						<EditField sort_order={props.task.sort_order} field_name={"name"} value={props.task.name} task_id={props.task.id} />
					</Box>
					<Box p={1} bgcolor="#f2f5f3" >
						<EditField sort_order={props.task.sort_order} field_name={"assignee"} value={props.task.assignee} task_id={props.task.id} />
					</Box>
					<Box p={1} bgcolor="#f2f5f3" >
						<EditField sort_order={props.task.sort_order} field_name={"due_date"} value={props.task.due_date} task_id={props.task.id} />
					</Box>
					<Box p={1} bgcolor="#f2f5f3" >
						<Checkbox inputProps={{ 'aria-label': 'checkbox with small size' }} size="small" checked={isComplete} onChange={handleChange} />
					</Box>
				</Box>
				
			}
		</Draggable>
	)
}

export default Item