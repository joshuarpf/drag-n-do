import React from 'react'
import Item from './Item'
import { Droppable } from 'react-beautiful-dnd'

const Column = (props: any) => {

	const displaylistItems = (props: any) => {
		if(props.listItems !== undefined) {
			return props.listItems.map((taskData: any, index: any) => (
				<Item key={taskData.id} task={taskData} index={index} />
			))
		} 
			
		return null
	}

	return(
		<div>
			<p>Here's your list of tasks</p>
			<Droppable droppableId="droppable-active-tasks">
			{(provided: any) => (
				<div id="task-container" ref={provided.innerRef} {...provided.droppableProps}>
						{displaylistItems(props)}
					{provided.placeholder}
				</div>
			)}
			</Droppable>
		</div>
	)
}

export default Column