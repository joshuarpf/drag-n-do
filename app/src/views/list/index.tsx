import React from 'react'
import { DragDropContext } from 'react-beautiful-dnd'
import { shallowEqual,  useDispatch, useSelector } from 'react-redux'

// Material UI 
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import UseStyles from '../common/'


// Components
import Column from './Column'
import AddForm from '../Forms'
import Container from '@material-ui/core/Container';

// Others
import TasksAction from '../../stores/tasks/TasksAction'

const List = () => {

	const classes = UseStyles();

	const dispatch = useDispatch()

	let tasksData = useSelector((state: any) => state.TasksData, shallowEqual);

	const reorder = (list: any, startIndex: any, endIndex: any) => {
		const result = Array.from(list);
		const [removed] = result.splice(startIndex, 1);
		result.splice(endIndex, 0, removed);
	
		return result;
	};
	
	/**
	 * Runs after an item has been dragged
	 * @param dragResult 
	 */
	const onDragEnd = (dragResult: any) => {
		const { destination, source } = dragResult

		if(!destination) { return }

		if(destination.droppable ===  source.droppable && destination.index === source.index){ return }

		const newList = reorder(
			tasksData.tasks, 
			dragResult.source.index,
      dragResult.destination.index
		)	

		const newSortOrderMap = newList.map((task: any, index: number) => ({ id: task.id, sort_order: index }))

		dispatch(TasksAction.updateOrder(newSortOrderMap))
	}

	return (
		<div>
			<div className={classes.root}>
			<CssBaseline />
			<AppBar>
				<Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title} align="center">
					Demo
				</Typography>
			</AppBar>
			<Container>
				<Grid container spacing={3}>
					<Grid item xs={12} md={8} lg={9}>
						<br />
						<br />
						<br />
						<AddForm />
					</Grid>
					<Grid item xs={12} md={8} lg={9}>
						<Paper className={classes.paper}>
							<DragDropContext  onDragEnd={onDragEnd}>
								<Column listItems={tasksData.tasks}/>
							</DragDropContext>
						</Paper>
					</Grid>
				</Grid>
			</Container>
		</div>
		</div>
	)
}

export default  List