import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import TasksActions from '../../stores/tasks/TasksAction'

// Material UI
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';


import UseStyles from '../common/'

interface EditFieldProps {
	field_name: string,
	value: string, 
	sort_order: number,
	task_id: number
}


const EditField = (props: EditFieldProps) => {

	const classes = UseStyles()
	const [editable, setEditable] = useState(false)
	const [currentValue, setCurrentValue] = useState(props.value)

	const handleChange = (value: any) => {
		setCurrentValue(value)
	}

	const dispatch = useDispatch()
	const handleClick = () => {
		setEditable(!editable)
	}

	const showElement = () => {
		if(editable === true) {
			const type = (props.field_name === 'due_date')? "date" : "text"
			return (<div>
				<TextField type={type} onChange={(input: any) => {handleChange(input.target.value)}} size="small" label={props.field_name} variant="standard" value={currentValue}/>
			</div>)
		}	else {
			return (<span style={{cursor: 'pointer'}} onClick={handleClick}>{currentValue}</span>)
		}
	}

	const hideElement = () => {
		setEditable(false)
	}

	const handleSave = () => {
		dispatch(TasksActions.update({ [props.field_name]: currentValue, id: props.task_id, sort_order: props.sort_order }))
		hideElement()
	}

	const customButtonStyle = {maxHeight: '20px', fontSize: '8px', maxWidth: '30px'}

	const showButtons = () => {
		if(editable === true)  {
			return (
				<div>
					<Button onClick={handleSave} style={customButtonStyle} variant="contained" color="primary">Confirm?</Button>
					&nbsp;
					<Button onClick={hideElement} style={customButtonStyle} variant="contained" color="secondary">Cancel</Button>
				</div>
			)
		}
	}

	return (
		<div className={classes.taskSection}>
			{showElement()}
			{showButtons()}
		</div>
	)

}

export default EditField