import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import TaskActions from '../../stores/tasks/TasksAction'

// Material UI 
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { FormControl } from '@material-ui/core'

export interface DialogProps {
	open: boolean, 
	onClose: () => void
}

const AddForm = (props: DialogProps) => {
	const { open, onClose } = props
	const [formvals, setFormvals] = useState({ name: '', assignee: '', due_date: '' })

	const handleChange = (event: any) => {
		setFormvals({
			...formvals,
			[event.target.id]: event.target.value
		})
	}

	const dispatch = useDispatch()

	const handleSave = () => {
		// TODO: impelment callt o validation
		dispatch(TaskActions.update(formvals))
		onClose()
	}

	const handleClose = () => {
		onClose()
	}


	return(
		<Dialog aria-labelledby="simple-dialog-title" open={open}>
			<DialogTitle>Task</DialogTitle>
			<DialogContent>
				<DialogContentText>
					Fill in the form to save a new task
				</DialogContentText>
				<FormControl>
				<TextField
            required
            margin="dense"
            id="name"
            label="Task name"
            type="text"
            fullWidth
						value={formvals.name}
						onChange={handleChange}
          />
					<TextField
						required
            margin="dense"
            id="assignee"
            label="Assignee"
            type="text"
            fullWidth
						value={formvals.assignee}
						onChange={handleChange}
          />
					<TextField
            required
            margin="dense"
            id="due_date"
            label=""
            type="date"
            fullWidth
						value={formvals.due_date}
						onChange={handleChange}
          />
					</FormControl>
			</DialogContent>
			<DialogActions>
				<Button variant="contained" color="primary" onClick={handleSave}>Save</Button>
				<Button variant="contained" color="secondary" onClick={handleClose}>Cancel</Button>
			</DialogActions>
		</Dialog>
	)
}

export default AddForm