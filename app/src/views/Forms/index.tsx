import React, { useState }  from 'react'

// Material UI 
import Button from '@material-ui/core/Button';

// Components
import AddForm from './Dialog'

const Forms = () => {
	const[open, setOpen] = useState(false)

	const handleClickOpen = () => {
		setOpen(true)
	}
	
	const handleClose = () => {
		setOpen(false)
	}

	return(
		<div>
			<Button variant="outlined" color="primary" onClick={handleClickOpen}>
				Add a task
			</Button>
			<AddForm open={open} onClose={handleClose}/>
		</div>
	)
}

export default Forms