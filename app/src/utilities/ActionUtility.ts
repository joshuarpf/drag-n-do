/**
 * Utility for creating actions
 */

export default class ActionUtility {
	static createAction(type: string, payload: any, error: boolean, meta: any) {
		return {
			type,
			payload,
			error, 
			meta
		}
	}
}