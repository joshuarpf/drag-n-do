/**
 * Class that contains methods to help process a "task" object
 */
export default class TaskUtility {
	/**
	 * Extracts the record id  from the given HTML element id
	 * @param elementID HTML element id
	 */
	static FetchDataIdFromElementId(elementID: string): number {
		const container	= elementID.split('-')

		return parseInt(container[1])
	}
}