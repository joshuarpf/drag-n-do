import { put } from 'redux-saga/effects'
import TasksActions from '../stores/tasks/TasksAction'
import TasksSelector from '../selectors/Tasks'

/**
 * Calls to delete tasks
 * @param action 
 */
export default function* DeleteTask(action: any) {
  yield put(TasksActions.showBusy())

  try {
    const taskID = action.payload.task_id
    yield TasksSelector.delete(taskID)
    .then(result => result.data)
    .catch(error => console.log(error))

    yield put(TasksActions.requestDeleteFinish(taskID))
  }catch (error) {
    console.log(`There was an error on saga -> deleteTask: ${error}`)
  }
}