import { all, takeEvery } from "redux-saga/effects";
import TasksActions from '../stores/tasks/TasksAction'

// Sagas
import DeleteTask from './Delete'
import SaveTask from './Save'
import GetAllTasks from './View'
import reorderTasks from './Reorder'


/**
 * Main action listener
 */
function* actionWatcher() {
  yield takeEvery(TasksActions.REQUEST_DELETE, DeleteTask);
  yield takeEvery(TasksActions.REQUEST_SHOW, GetAllTasks);
  yield takeEvery(TasksActions.REQUEST_UPDATE, SaveTask)
  yield takeEvery(TasksActions.REQUEST_UPDATE_ORDER, reorderTasks)
}

/**
 * Root saga
 */
export default function* rootSaga() {
  yield all([
    actionWatcher(),
  ]);
}