import { put } from 'redux-saga/effects'
import TasksActions from '../stores/tasks/TasksAction'
import TasksSelector from '../selectors/Tasks'

/**
 * Calls to update tasks
 * @param action 
 */
export default function* SaveTask(action: any) {
  yield put(TasksActions.showBusy())
  
  try{

    const taskProperties = action.payload
    const data = yield TasksSelector.update(taskProperties)
    .then(result => result.data)
    .catch(error => {
      console.error(error)
    })

    if(data) {
      if(taskProperties.id === undefined) { // New 
        yield put(TasksActions.insertNew(data))
      } else { // Update
        yield put(TasksActions.updateFinish(data))
      }
    }

  } catch(error) {
    yield put(TasksActions.showNotBusy())
  }
}