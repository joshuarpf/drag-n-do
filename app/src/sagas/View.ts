import { put } from 'redux-saga/effects'
import TasksActions from '../stores/tasks/TasksAction'
import TasksSelector from '../selectors/Tasks'

/**
 * Executes the process of viewing all tasks
 */
export default function* GetAllTasks() {
  yield put(TasksActions.showBusy())

  try{
    const data = yield TasksSelector.fetch() 
    .then(result => result.data)
    .catch(error => {
      console.error(error)
    })
    
    yield put(TasksActions.requestShowFinish(data))
    
  } catch(error) {
    yield put(TasksActions.showNotBusy())
  }

}