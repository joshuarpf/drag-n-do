import ActionUtility from '../../utilities/ActionUtility'
/**
 * Every action on tasks
 */
export default class TasksAction {
	static REQUEST_SHOW = 'TasksAction.REQUEST_SHOW'
	static REQUEST_BUSY = 'TasksAction.REQUEST_BUSY'
	static REQUEST_SHOW_FINISHED = 'TasksAction.REQUEST_SHOW_FINISHED'
	static REQUEST_NOT_BUSY = 'TasksAction.REQUEST_NOT_BUSY'
	static REQUEST_UPDATE = 'TaskAction.REQUEST_UPDATE'
	static REQUEST_UPDATE_FINISHED = 'TaskAction.REQUEST_UPDATE_FINISHED'
	static REQUEST_UPDATE_ORDER = 'TaskAction.REQUEST_UPDATE_ORDER'
	static REQUEST_UPDATE_ORDER_FINISHED = 'TaskAction.REQUEST_UPDATE_ORDER_FINISHED'
	static REQUEST_DELETE = 'TaskAction.REQUEST_DELETE'
	static REQUEST_DELETE_FINISHED = 'TaskAction.REQUEST_DELETE_FINISHED'

	// For Reducers
	static INSERT_NEW = 'TaskAction.INSERT_NEW'

	static insertNew = (task: any): any => (
		ActionUtility.createAction(TasksAction.INSERT_NEW, task, false, "ACTION -> INSERT_NEW")
	)

	static requestDelete = (id: number): any => (
		ActionUtility.createAction(TasksAction.REQUEST_DELETE, { task_id: id }, false, "ACTION -> REQUEST_DELETE")
	)

	static requestDeleteFinish = (id: number):any => (
		ActionUtility.createAction(TasksAction.REQUEST_DELETE_FINISHED, id, false, "ACTION -> REQUEST_DELETE_FINISHED")
	)

	static requestShow = (itemsPerPage = 10): any => (
		ActionUtility.createAction(TasksAction.REQUEST_SHOW, itemsPerPage, false, "ACTION -> REQUEST_SHOW")
	)

	static requestShowFinish = (tasks: any[]):any => (
		ActionUtility.createAction(TasksAction.REQUEST_SHOW_FINISHED, tasks, false, "ACTION -> REQUEST_SHOW_FINISHED")
	)

	static showBusy = (): any => (
		ActionUtility.createAction(TasksAction.REQUEST_BUSY, {}, false, "ACTION -> REQUEST_BUSY")
	)

	static showNotBusy = (): any => (
		ActionUtility.createAction(TasksAction.REQUEST_NOT_BUSY, {}, false, "ACTION -> REQUEST_NOT_BUSY")
	)

	static update = (updateData: {}): any => (
		ActionUtility.createAction(TasksAction.REQUEST_UPDATE, updateData, false, "ACTION -> REQUEST_UPDATE")
	)

	static updateFinish = (updateData: {}): any => (
		ActionUtility.createAction(TasksAction.REQUEST_UPDATE_FINISHED, updateData, false, "ACTION -> REQUEST_UPDATE_FINISHED")
	)

	static updateOrder = (orderData: {}): any => (
		ActionUtility.createAction(TasksAction.REQUEST_UPDATE_ORDER, orderData, false, "ACTION -> REQUEST_UPDATE_ORDER")
	)

	static updateOrderFinished = (tasks: {}): any => (
		ActionUtility.createAction(TasksAction.REQUEST_UPDATE_ORDER_FINISHED, tasks, false,  "ACTION -> REQUEST_UPATE_ORDER_FINISHED")
	)
}