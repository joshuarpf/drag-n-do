import TasksAction from './TasksAction'
import BaseReducer from '../../utilities/BaseReducer'

export default class TasksReducer extends BaseReducer{

  [TasksAction.INSERT_NEW](state: any, action: any) {
    return {
      ...state, 
      tasks: [...state.tasks, action.payload]
    }
  }

  [TasksAction.REQUEST_DELETE_FINISHED](state: any, action: any) {
    const deleted_id = action.payload
    const remainingTasks = state.tasks.filter((task: any) => (task.id !== deleted_id))
    return {
      ...state, 
      tasks: remainingTasks
    }
  }

  [TasksAction.REQUEST_SHOW_FINISHED](state: any , action: any) {
    return {
      ...state, 
      status: "", 
      tasks: action.payload
    }
  }

  [TasksAction.REQUEST_BUSY](state: any, action: any) {
    return {
      ...state,
      status: "busy", 
    }
  }

  [TasksAction.REQUEST_UPDATE_FINISHED](state: any, action: any) {
    return {
      ...state,
      tasks: state.tasks.map((task: any) => (task.id === action.payload.id)? {
        ...task, 
        name: action.payload.name, 
        assignee: action.payload.assignee, 
        task_status: action.payload.task_status, 
        sort_order: action.payload.sort_order, 
        due_date: action.payload.due_date }
        : task ) 
    }
  }

  [TasksAction.REQUEST_UPDATE_ORDER_FINISHED](state: any, action: any) {
    console.log('updating reducer .... ')
    console.log(action.payload)
    return {
      ...state, 
      status: 'reorderd', 
      tasks: action.payload
    }
  }
}