import { combineReducers } from 'redux'
import TasksReducer from './tasks/TasksReducer'

export default combineReducers({
	TasksData: new TasksReducer().reducer
})