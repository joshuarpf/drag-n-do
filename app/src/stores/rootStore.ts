import { applyMiddleware, compose, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'
import root from '../sagas'

import rootReducer from './rootReducer'

declare global {
	interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }	
}

const initializeSagaMiddleware = createSagaMiddleware();
const storeEnhancers = ( window && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const Store = createStore(
	rootReducer, 
	storeEnhancers(
		applyMiddleware(initializeSagaMiddleware)
	)
)

initializeSagaMiddleware.run(root)

export default Store