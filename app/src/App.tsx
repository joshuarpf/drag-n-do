import React, { useEffect }from 'react';
import { useDispatch } from 'react-redux'
import List from './views/list/'
import TasksActions from './stores/tasks/TasksAction'

const App = () => {

  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(TasksActions.requestShow())
  })

  return (
    <div className="App">
      <List />
    </div>
  );
}

export default App
