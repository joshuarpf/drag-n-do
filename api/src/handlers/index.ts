import { request, Request, response, Response } from 'express'
import crud from '../services/crud'
import DataProcessor from '../services/DataProcessor'
// TODO: wrap the return for a better structure

/**
 * handler for delete
 * @param request 
 * @param response 
 */
export const deleteTask = (request: Request, response: Response) => {

	const { id } = request.body
	var deleteResult = crud.delete(id)

	if(deleteResult) {
		response.json({ success: true, deleted_id: id })
	} else {
		response.json({ success: false })
	}
}

/**
 * Hnadler for laoding up the tasks collection
 * @param request 
 * @param response 
 */
export const load = async (request: Request, response: Response) => {
	var tasks = await crud.fetchTodos()
	response.json({ sucess: true, data: tasks })
}

/**
 * Handler for the save  endpoit 
 * @param request 
 * @param response 
 */
export const save = async (request: Request, response: Response) => {
	const { id, name, assignee, task_status, sort_order, due_date } = request.body

	const derivedSortOrder = await DataProcessor.getNewSortOrder()

	const saveResult = await crud.save(
		name, 
		assignee, 
		(task_status === undefined)? 0:task_status, 
		(sort_order === undefined)? parseInt(derivedSortOrder): sort_order, 
		due_date,
		(id > 0)? id:null
	)

	if(saveResult) {
		response.json({ success: true, data: saveResult })
	}
	
}

/**
 * Handler for the sorting endpoint
 * @param request 
 * @param response 
 */
export const sort = async(request: Request, response: Response) => {
	const newOrder = request.body
	if(newOrder) {

		await Promise.all(newOrder.map(async (orderBasis: any) => {
			const updatedData = await crud.update({ sort_order: orderBasis.sort_order }, orderBasis.id )
		}))

		load(request, response)
	
	} else {
		response.json({ success: false, error: 'No sorted map derrived' })
	}

}