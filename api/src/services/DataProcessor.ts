
import { Task } from '../models'
export default class DataProcessor {

	/**
	 * Returns an integer that represents the new sort order to be used. 
	 * This is derived by querying the last last sort order from the tasks, and 
	 * incrementing the value.
	 */
	static getNewSortOrder = async(): Promise<any> => {
		return await Task.findOne({ order:[['sort_order', 'DESC']]})
		.then((data: any) => {
			if(data) {
				let value = parseInt(data.sort_order) + 1 
				return value		
			} else {
				return 0
			}
		})
		.catch(error => {
			console.log(`ERROR ON FETCHING NEW SORT ORDER: ${error}`)
		})
	}
	
	/**
	 * Returns a collection of objects with id and sort order, representing the task IDs and their new sorting order. 
	 * @param baseId 
	 * @param baseSortOrder 
	 * @param tasks 
	 */
	static getSortedMap = (baseId: number, baseSortOrder: number, tasks: any) => {
		
		/**
			* Keeps an updated array of sorted IDs
			*/
			const newSortingContainer = []
			for(var iterator = 0; iterator < tasks.length; iterator++) {
				const { id, sort_order } = tasks[iterator]
				
				if(id != baseId) {
					if(sort_order == baseSortOrder) {
						newSortingContainer.push(baseId)
					}

					newSortingContainer.push(id)
				}
			}

			const sortedMap = DataProcessor.buildNVP(newSortingContainer)

		return sortedMap
	}

	/**
	 * Returns a collection of objects derived from the provided collection of ids
	 */
	private static buildNVP = (ids: number[]): any => {
		const container = []
		for(var iterator = 0; iterator < ids.length; iterator++) {
			container.push({id: ids[iterator], sort_order: iterator})
		}

		return container
	}
}