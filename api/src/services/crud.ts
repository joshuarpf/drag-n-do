import { Task } from '../models'
class CRUD {
	constructor() {}

	creatNew(properties: any): Promise<any> {
		const task = new Task(properties)

		const saveResult = task.save()
		.then(result => {
			return result
		})
		.catch(error => {
			console.log(`ERROR: ${error}`)
		})

		return saveResult
	}

	async delete(id: number): Promise<any> {
		return await Task.destroy({ where: { id: id }})
		.then(result => {
			return true
		})
		.catch(error => {
			console.log(`CRUD delete ERROR: ${error}`)
			return false
		})
	}

 	fetchTodos(): Promise<any> {
		return Task.findAll({order:[['sort_order', 'ASC']]}).then((tasks) => {
			return tasks
		}).catch(error => {
			console.log(`CRUD -> findAll ERROR: ${error}`)
		})
	}

	save(name: string, assignee: string, task_status: string, sort_order: number, due_date: Date, id?: number): any {
		
		if(id) { // Call to update
			return this.update({name,  assignee,  task_status, sort_order,  due_date}, id)
		} else {
			return this.creatNew({ name,  assignee, sort_order, due_date, task_status: 0 })
		}
	}

	update(properties: any, id: number): Promise<any> {
		return Task.update(properties, { where:{ "id": id } })
		.then(async (task) => {
			return await Task.findOne({ where: { "id": id }})
		}).catch(error => {
			console.log(`CRUD -> save (update) ERROR: ${error}`)
		})
	}
}

const crud = new CRUD()

export default crud