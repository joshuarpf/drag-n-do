import { BuildOptions, Model } from 'sequelize'

export interface TasksInterface{
	id: number,
	name: string, 
	assignee: string, 
	task_status: string, 
	sort_order: int, 
	due_date: Date
}

export interface TaskModel extends Model<TasksInterface>, TaskModel{}

export class Task extends Model<TasksModel, TasksInterface>{}

export type TaskStatic = typeof Model & {
	new(values?: object, options?: BuildOptions): TaskModel
}