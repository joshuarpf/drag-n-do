import { DataTypes, Sequelize }from 'sequelize'
import { TaskStatic } from '../types/index'

/**
 * returns a static model of tasks
 * @param sequelize 
 */
export function TaskFactory(sequelize: Sequelize): TaskStatic {
	return <TaskStatic>sequelize.define("tasks", {
		id: {
			type: DataTypes.INTEGER, 
			autoIncrement: true, 
			primaryKey: true,
		}, 
		name: {
			type: DataTypes.STRING, 
			allowNull: false
		}, 
		assignee: {
			type: DataTypes.STRING, 
			allowNull: false
		},
		task_status: {
			type: DataTypes.STRING,
			allowNull: false
		}, 
		sort_order: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		due_date: {
			type: DataTypes.DATE, 
			allowNull: false
		}, 
	}, { timestamps: false })
}