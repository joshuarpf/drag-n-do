	// Imports
	import * as sequelize from 'sequelize'
	import { TaskFactory } from './tasks'

	/**
	 * Creatse the database connection instance
	 */
	export const dbConfig = new sequelize.Sequelize(
		`${process.env.DEV_DB_HOST_IP}`, 
		`${process.env.DEV_DB_USER}`,
		`${process.env.DEV_DB_PASSWORD}`, 
		{
			port: 5432,
			host: '10.6.0.8',
			dialect: 'postgres',
			pool: {
				min:0, 
				max:5, 
				acquire: 30000,
				idle: 10000,
			}
		}
	)

	export const Task = TaskFactory(dbConfig)