// Imports
import * as dotenv from 'dotenv'
import express from 'express'
import cors from "cors"
import helmet from "helmet"
import { deleteTask, load, save, sort } from './handlers'

dotenv.config()

async function  startServer() {

	const app = express()
	// Initialization of the app
	app.use(helmet())
	app.use(cors())
	app.use(express.json())

	app.get('/', load)
	app.post('/save', save)
	app.post('/sort', sort)
	app.post('/remove', deleteTask)

	app.listen(process.env.PORT, () => {
		console.log(`Server is running on: http://${process.env.HOST}:${process.env.PORT}`)
	})

}

startServer()
