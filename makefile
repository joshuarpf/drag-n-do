default: 
	@echo "============= building Local DEV API ============="
	docker-compose up -d --build
	docker-compose logs -f

clean: 
	@echo "============= Cleaning up dev environment ============="
	docker-compose down
	docker container prune -f
	docker rmi dragndo_api
	docker rmi dragndo_postgres