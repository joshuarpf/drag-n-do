CREATE TABLE tasks (
	id serial PRIMARY KEY,
	name VARCHAR (250),
	assignee VARCHAR (50) NOT NULL,
	task_status INTEGER DEFAULT (0),
	sort_order INT,
	due_date DATE
);